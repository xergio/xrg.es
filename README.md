Code for [RegExp Live tester](https://xrg.es).

Install: `npm i`

Dev server: `npm start`

Build: `npm run build`

Simple nginx dev server:

```
server {
        listen 80;
        server_name localhost;

        root /var/www/;

        location ~ \.php$ {
                fastcgi_pass unix:/run/php/php7.2-fpm.sock;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $request_filename;
        }
}
```

And then:

```bash
$ mkdir /var/www/xrg.es
$ sudo mount --bind api/ /var/www/xrg.es
```

Deploy to heroku: `git push heroku master`
 
