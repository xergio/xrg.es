<?php

require_once dirname(__FILE__) ."/redis.php";
require_once dirname(__FILE__) ."/preg.php";

function response($data) {
	header("Content-Type: application/json");
	die(json_encode($data));
}

try {
	if ($_SERVER['HTTP_HOST'] !== 'xrg.es') { // dev
		// $ sudo mount --bind api/ ~/www/xrg.es
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");
		header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS");

	} else if (!preg_match("~^https://".preg_quote($_SERVER["HTTP_HOST"])."/~", $_SERVER["HTTP_REFERER"])) {
		throw new Exception("Invalid request");
	}

	// thank you php...
	$post = json_decode(file_get_contents("php://input"), true);

	if ($post['hash']) {
		$key = mb_substr($post['hash'], 1, 10);
		$state = $redis->hGetAll("xrg.es:". $key);
		$redis->expire("xrg.es:". $key, 60*60*24*7); // touch
		response($state);
	}

	if (!is_array($post)) {
		die("{}");
	}

	$preg = new Preg($post);
	$result = $preg->exec();

	$key = $preg->hash();
	$redis->hMSet("xrg.es:". $key, $post);
	$redis->expire("xrg.es:". $key, 60*60*24*7);

	response(array_merge((array)$result, ['hash' => $key]));

} catch (Throwable $e) {
	response(['fatal' => $e->getMessage()]);
}
