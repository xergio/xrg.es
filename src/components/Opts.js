import React, { Component } from 'react';
import OptsMatch from './opts/OptsMatch';
import OptsMatchAll from './opts/OptsMatchAll';
import OptsSplit from './opts/OptsSplit';
import OptsReplace from './opts/OptsReplace';
import OptsQuote from './opts/OptsQuote';

export default class Opts extends Component {
  render() {
    const { method } = this.props

    if (method === 'preg_match') {
      return <OptsMatch {...this.props} />

    } else if (method === 'preg_match_all') {
      return <OptsMatchAll {...this.props} />

    } else if (method === 'preg_split') {
      return <OptsSplit {...this.props} />

    } else if (method === 'preg_replace') {
      return <OptsReplace {...this.props} />

    } else if (method === 'preg_quote') {
      return <OptsQuote {...this.props} />
    }

    return <div />
  }
}
