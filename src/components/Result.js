import React, { Component } from 'react'

function mapObject(object, callback) {
  return Object.keys(object).map(key => {
    return callback(key, object[key])
  })
}

const DumpList = (props) => {
  const type = typeof props.dump

  if (type === "object") {
    if (Object.keys(props.dump).length === 0) {
      return <tt className="text-secondary">[]</tt>
    }

    const items = mapObject(props.dump, (key, value) => {
      if (['number', 'string'].includes(typeof value)) {
        // .replace(" ", '⎵')
        return <tr key={key}><td className="bg-gray text-right keys">{key}</td><td>{value}</td></tr>
      } else {
        return <tr key={key}><td className="bg-gray text-right keys">{key}</td><td><DumpList dump={value} indent={props.indent + 1} /></td></tr>
      }
    })

    return (
      <table>
        <tbody>
          {items}
        </tbody>
      </table>
    )

  } else if (type === 'string') {
    return (<tt>{props.dump}</tt>)
  }

  return (<div><em>empty result</em></div>)
}

const Fatal = (props) => {
  if (!props.fatal) {
    return <div />
  }

  return (
    <div className="toast toast-error">
      {props.fatal}
    </div>
  )
}

export default class Result extends Component {
  render() {
    return (
      <div className="columns">
        <div className="column col-12">
          <div className="divider" data-content="$matches or result"></div>
          <Fatal fatal={this.props.response.fatal} />
          <DumpList dump={this.props.response.dump} indent={0} />
        </div>
      </div>
    )
  }
}
