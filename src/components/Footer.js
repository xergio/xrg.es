import React, { Component } from 'react';

class IconLink extends Component {
  constructor(props) {
    super(props)

    this.state = {
      spin: 10,
      noanimate: false
    }
  }

  componentDidMount() {
    if (this.state.noanimate) {
      return
    }
    //this.tick()
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  tick = () => {
    this.setState({ spin: this.state.spin + (Math.floor(Math.random() * Math.floor(720))) - 360 }, () => {
      clearInterval(this.timerID)
      this.timerID = setTimeout(() => this.tick(), 60000)
    })
  }

  render() {
    const css = {
      transform: 'rotate('+this.state.spin+'deg)' 
    }

    return (
      <span>
        <a href={this.props.href} className="tooltip" data-tooltip={this.props.alt} onMouseEnter={this.tick}>
          <svg style={css}><use xlinkHref={'/img/symbol-defs.svg#'+this.props.src} /></svg>
          <span>{this.props.alt}</span>
        </a>
      </span>
    )
  }
}

export default class Footer extends Component {
  render() {
    return (
      <footer className="columns">
        <div className="column col-12">
          <p className="text-right">
            <small>
              <span>Coded by <a href="https://twitter.com/xergio">Sergio Álvarez</a> – </span>
              <span><IconLink href="https://reactjs.org/" src="icon-react" alt="React" />  </span>
              <span><IconLink href="https://picturepan2.github.io/spectre/" src="icon-spectre" alt="Spectre" /> </span>
              <span><IconLink href="https://php.net/manual/es/ref.pcre.php" src="icon-php" alt="PHP" /> </span>
              <span><IconLink href="https://redis.io/" src="icon-redis" alt="Redis" /> </span>
              <span><IconLink href="https://gitlab.com/xergio/xrg.es/issues" src="icon-gitlab" alt="Gitlab" /> </span>
            </small></p>
        </div>
      </footer>
    )
  }
}
