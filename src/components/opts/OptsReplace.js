import React, { Component } from 'react';
import Limit from '../widgets/Limit';

export default class OptsReplace extends Component {
  render() {
    return [
      <div className="form-group" key="limit">
        <div className="col-3 col-mr-auto">
          <Limit limit={this.props.limit} onChange={this.props.onChange} />
        </div>
      </div>
    ]
  }
}
