import React, { Component } from 'react';
import Checkbox from '../widgets/Checkbox';
import Offset from '../widgets/Offset';

export default class OptsMatchAll extends Component {
  render() {
    return [
      <div className="form-group" key="ppo">
        <p className="form-input-hint"><tt>PREG_PATTERN_ORDER</tt> is the default order method.</p>
      </div>,
      <div className="form-group" key="pso">
        <Checkbox label="PREG_SET_ORDER" name="PREG_SET_ORDER" checked={this.props.PREG_SET_ORDER} onChange={this.props.onChange} />
      </div>,
      <div className="form-group" key="poc">
        <Checkbox label="PREG_OFFSET_CAPTURE" name="PREG_OFFSET_CAPTURE" checked={this.props.PREG_OFFSET_CAPTURE} onChange={this.props.onChange} />
      </div>,
      <div className="form-group" key="puan">
        <Checkbox label="PREG_UNMATCHED_AS_NULL" name="PREG_UNMATCHED_AS_NULL" checked={this.props.PREG_UNMATCHED_AS_NULL} onChange={this.props.onChange} />
      </div>,
      <div className="form-group" key="offset">
        <div className="col-3 col-mr-auto">
          <Offset offset={this.props.offset} onChange={this.props.onChange} />
        </div>
      </div>
    ]
  }
}
