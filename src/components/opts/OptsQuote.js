import React, { Component } from 'react';

export default class OptsQuote extends Component {
  render() {
    return [
      <div className="form-group" key="delimiter">
        <div className="col-3 col-mr-auto">
          <div className="input-group">
            <span className="input-group-addon">delimiter</span>
            <input className="form-input" type="text" name="delimiter" value={this.props.delimiter} onChange={this.props.onChange} />
          </div>
        </div>
      </div>
    ]
  }
}
