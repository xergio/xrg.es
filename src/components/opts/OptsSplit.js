import React, { Component } from 'react';
import Checkbox from '../widgets/Checkbox';
import Limit from '../widgets/Limit';

export default class OptsSplit extends Component {
  render() {
    return [
      <div className="form-group" key="limit">
        <div className="col-3 col-mr-auto">
          <Limit limit={this.props.limit} onChange={this.props.onChange} />
        </div>
      </div>,
      <div className="form-group" key="psne">
        <Checkbox label="PREG_SPLIT_NO_EMPTY" name="PREG_SPLIT_NO_EMPTY" checked={this.props.PREG_SPLIT_NO_EMPTY} onChange={this.props.onChange} />
      </div>,
      <div className="form-group" key="psdc">
        <Checkbox label="PREG_SPLIT_DELIM_CAPTURE" name="PREG_SPLIT_DELIM_CAPTURE" checked={this.props.PREG_SPLIT_DELIM_CAPTURE} onChange={this.props.onChange} />
      </div>,
      <div className="form-group" key="psoc">
        <Checkbox label="PREG_SPLIT_OFFSET_CAPTURE" name="PREG_SPLIT_OFFSET_CAPTURE" checked={this.props.PREG_SPLIT_OFFSET_CAPTURE} onChange={this.props.onChange} />
      </div>
    ]
  }
}
