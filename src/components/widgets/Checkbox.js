import React, { Component } from 'react';

export default class Checkbox extends Component {
  render() {
    return (
      <label className="form-checkbox">
        <input type="checkbox" name={this.props.name} checked={this.props.checked} onChange={this.props.onChange} />
        <i className="form-icon"></i> {this.props.label}
      </label>
    )
  }
}
