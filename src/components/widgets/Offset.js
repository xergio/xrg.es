import React, { Component } from 'react';

export default class Offset extends Component {
  render() {
    return (
      <div className="input-group">
        <span className="input-group-addon">offset</span>
        <input className="form-input" type="text" placeholder="0" name="offset" value={this.props.offset} onChange={this.props.onChange} />
      </div>
    )
  }
}
