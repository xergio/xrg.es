import React, { Component } from 'react';

export default class Limit extends Component {
  render() {
    return (
      <div className="input-group">
        <span className="input-group-addon">limit</span>
        <input className="form-input" type="text" placeholder="-1" name="limit" value={this.props.limit} onChange={this.props.onChange} />
      </div>
    )
  }
}
