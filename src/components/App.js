import React, { Component } from 'react';
import axios from 'axios'
import Clipboard from 'react-clipboard.js';
import Tabs from './Tabs';
import Fields from './Fields';
import Opts from './Opts';
import Snippet from './Snippet';
import Result from './Result';
import Footer from './Footer';
import Help from './Help';

/*

~(?P<fsda>\d+)~
~(?P<fsda>\w+)~
http://localhost:3000/#hggc4o
http://localhost:3000/#d4htjo
http://localhost:3000/#mxj9wu
http://localhost:3000/

*/

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      method: 'preg_match',
      pattern: '',
      replacement: '',
      subject: '',
      offset: '',
      limit: '',
      delimiter: '',
      PREG_OFFSET_CAPTURE: false,
      PREG_UNMATCHED_AS_NULL: false,
      PREG_SET_ORDER: false,
      PREG_SPLIT_NO_EMPTY: false,
      PREG_SPLIT_DELIM_CAPTURE: false,
      PREG_SPLIT_OFFSET_CAPTURE: false,
      response: {}
    }

    this.timer = null
    this.apiUrl = window.location.hostname === 'xrg.es'? 
      'https://xrg.es/api': 
      'http://'+ window.location.hostname +'/xrg.es/main.php' // dev
  }

  componentDidMount() {
    window.addEventListener("hashchange", this.hashChange, false)
    this.hashChange()
  }

  componentWillUnmount() {
    window.removeEventListener("hashchange", this.hashChange, false)
  }

  hashChange = () => {
    const hash = window.location.hash

    if (hash && hash !== '#close' && hash !== '#help') {
      this.setState({ loading: true }, () => {
        axios.post(this.apiUrl, { hash })
        .then((res) => {
          this.setState({ ...res.data, response: {}, loading: false }, () => { this.api() })
        })
      })
    }
  }

  api = () => {
    // exclude this keys
    const { loading, response, ...state } = this.state

    this.setState({ loading: true }, () => {
      axios.post(this.apiUrl, state)
      .then((res) => {
        this.setState({ response: res.data, loading: false })
      })
    })
  }

  delay = () => {
    clearTimeout(this.timer)
    this.timer = setTimeout(this.api, 200)
  }

  methodSelect = (method) => {
    this.setState({method}, this.api)
  }

  handleInputChange = (event) => {
    const target = event.target
    const value = (target.type === 'checkbox')? target.checked: target.value
    const name = target.name

    this.setState({ [name]: value }, this.delay)
  }

  render() {
    const { method, pattern, replacement, subject, response, spin, ...state } = this.state
    const hash = response.hash? response.hash: ''
    const clipb = hash? <Clipboard className="btn tooltip tooltip-bottom" data-tooltip="Copy permalink" data-clipboard-text={"https://xrg.es/#"+hash}><i className="icon icon-link"></i></Clipboard>: <span />

    return (
      <div className="container">
        <header className="navbar">
          <section className="navbar-section">
            <svg><use xlinkHref="/img/symbol-defs.svg#icon-php-alt" /></svg>
            <a href="https://xrg.es" className="navbar-brand">Regular Expressions tester</a>
          </section>
          <section className="navbar-section">
            {clipb}
            <a href={'https://php.net/'+method} className="btn btn-link">docs</a>
            <a href="#help" className="btn btn-primary">help</a>
          </section>
        </header>

        <Tabs method={method} onChange={this.methodSelect} />
        <form>
          <Fields method={method} pattern={pattern} replacement={replacement} subject={subject} onChange={this.handleInputChange} />
          <div className="divider" data-content="options"></div>
          <Opts method={method} {...state} onChange={this.handleInputChange} />
        </form>
        <Snippet method={method} response={response} />
        <Result method={method} response={response} />
        <Footer />
        <Help />
      </div>
    );
  }
}
