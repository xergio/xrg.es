import React, { Component } from 'react';

export default class Fields extends Component {
  render() {
    const labels = {
      'pattern': 'Regular Expression / Pattern', 
      'replacement': 'Replacement', 
      'subject': 'String / Subject'
    }

    let field_names
    if (this.props.method === 'preg_replace') {
      field_names = ['pattern', 'replacement', 'subject']
    } else if (this.props.method === 'preg_quote') {
      field_names = ['pattern']
    } else {
      field_names = ['pattern', 'subject']
    }

    const fields = field_names.map((name) =>  {
      return (
        <div className="form-group" key={name}>
          <label className="form-label" htmlFor={'field-'+name}>{labels[name]}</label>
          <input className="form-input" type="text" id={'field-'+name} name={name} value={this.props[name]} placeholder={name} onChange={this.props.onChange} />
        </div>
      )
    })

    return fields
  }
}
