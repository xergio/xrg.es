import React, { Component } from 'react';

function createMarkup(html) {
  return {__html: html}
}

const Code = (props) => {
  if (!props.code) {
    return <div />
  }

  return (
    <div>
      <div className="divider" data-content="code"></div>
      <p><span dangerouslySetInnerHTML={createMarkup(props.code)} /></p>
    </div>
  )
}

const Result = (props) => {
  if (props.type === undefined) {
    return <div />
  }
  
  return (
    <div>
      <div className="divider" data-content="return"></div>
      <p><code>{props.type}</code> <code>{props.value}</code></p>
    </div>
  )
}

export default class Snippet extends Component {
  render() {
    return (
      <div className="columns">
        <div className="column col-2">
          <Result type={this.props.response.return_type} value={this.props.response.return_value} />
        </div>
        <div className="column col-10">
          <Code code={this.props.response.code} />
        </div>
      </div>
    )
  }
}

