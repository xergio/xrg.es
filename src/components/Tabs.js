import React, { Component } from 'react';

export default class Tabs extends Component {
  render() {
    const functions = ['preg_match', 'preg_match_all', 'preg_split', 'preg_replace', 'preg_quote']
    const tabs = functions.map((func) =>  {
      const cn = func === this.props.method? "active": "";

      return (
        <li className={'tab-item '+cn} key={func}>
          <a href={'#'+func} onClick={(e) => {
            e.preventDefault()
            this.props.onChange(func) 
          }}>{func}</a>
        </li>
      )
    })

    return (
      <div className="columns">
        <div className="column col-12 col-mx-auto">
          <ul className="tab tab-block">
            {tabs}
          </ul>
        </div>
      </div>
    );
  }
}
